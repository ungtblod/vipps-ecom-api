<!-- START_METADATA
---
draft: true
---
END_METADATA -->

# Outdated information

The PDFs in this directory are severely outdated, and provided for reference only.

See https://github.com/vippsas/vipps-ecom-api for the current documentation.
