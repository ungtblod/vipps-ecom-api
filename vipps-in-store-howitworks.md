<!-- START_METADATA
---
title: How it works in the store
sidebar_position: 9
---
END_METADATA -->

# Vipps In Store: How It Works

<!-- START_COMMENT -->

ℹ️ Please use the new documentation:
[Vipps Technical Documentation](https://vippsas.github.io/vipps-developer-docs/).

<!-- END_COMMENT -->

Vipps i store is designed for stores that want to let customers pay easily and quickly from their own phone, without the need to touch the terminal. This page shows an example of how you can offer contactless payment to your customers by integrating Vipps in your POS system.


It is also possible to use our simpler solution Vippsnummer, but then the amount must be entered manually at checkout, and there will be some follow-up with accounting and settlement.


## The payment process in store

![In store process](images/vipps-in-store-process.svg)


## 1. Add products to sell

Add the products that the customer wants to buy in the POS system.

![The POS system](images/vipps-in-store-step1.svg)

## 2. Enter the customer's phone number

When the customer is ready to pay, choose “Pay with Vipps”, and enter the customer’s phone number.

![Enter phone number](images/vipps-in-store-step2.svg)

## 3. The customer confirms payment in Vipps

The customer confirms the payment in Vipps on their phone.

![Confirm payment](images/vipps-in-store-step3-2.svg)

## 4. Payment is registered

The payment is registered in the POS system.

![Payment confirmation](images/vipps-in-store-step4.svg)


## Great! Now you know how the Vipps in store payment process works.

Take a look at the technical documentation in the [Vipps eCommerce API Guide](vipps-ecom-api.md)


## Questions?

For information in Norwegian, see: [Vipps i kassa (POS)](https://vipps.no/produkter-og-tjenester/bedrift/ta-betalt-i-butikk/vipps-i-kassa/).

We're always happy to help with code or other questions you might have!
Please create an [issue](https://github.com/vippsas/vipps-ecom-api/issues),
a [pull request](https://github.com/vippsas/vipps-ecom-api/pulls),
or [contact us](https://github.com/vippsas/vipps-developers/blob/master/contact.md).

Sign up for our [Technical newsletter for developers](https://github.com/vippsas/vipps-developers/tree/master/newsletters).
